import axios from 'axios'

export const HTTP = axios.create({
  baseURL: 'http://10.1.10.160:8025/',
  headers: {
    Authorization: `${window.localStorage.getItem('token')}`
  }
})
export default HTTP
