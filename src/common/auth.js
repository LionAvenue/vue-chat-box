import HTTP from './HTTP'

/**
 * Check current token
 *
 * @returns {*|AxiosPromise<any>|void}
 */
const check = () => {
  return HTTP.post('/api/user/auth', { token: localStorage.getItem('token') })
}

/**
 * Check authentication if token is invalid redirect to /
 * else allow request through guard
 *
 * @param to
 * @param from
 * @param next
 * @returns {Promise<void>}
 */
const auth = async (to, from, next) => {
  try {
    if (!(await check()).data.success) {
      window.localStorage.removeItem('token')
      next({ path: '/login' })
    } else {
      next()
    }
  } catch (error) {
    window.localStorage.removeItem('token')
    next({ path: '/login' })
  }
}

export {auth, check}
