import Vue from 'vue'
import Router from 'vue-router'
import Board from '@/components/Board'
import Login from '@/components/Login'
import Music from '@/components/Music'
import { auth } from '../common/auth'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Music',
      component: Music,
      beforeEnter: auth
    },
    {
      path: '/Board',
      name: 'Board',
      component: Board,
      beforeEnter: auth
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
