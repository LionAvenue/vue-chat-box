// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import VueLodash from 'vue-lodash'
import 'vuetify/dist/vuetify.min.css'
import VueChatScroll from 'vue-chat-scroll'
import VueSocketio from 'vue-socket.io'
import VueSweetalert2 from 'vue-sweetalert2'

Vue.use(VueSweetalert2)
Vue.use(VueSocketio, 'http://10.1.10.160:8025/')
Vue.use(VueChatScroll)
Vue.use(Vuetify)
Vue.use(VueLodash)
Vue.config.productionTip = false

export const bus = new Vue()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
